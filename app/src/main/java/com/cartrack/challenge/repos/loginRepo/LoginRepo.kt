package com.cartrack.challenge.repos.loginRepo

import com.cartrack.challenge.dataManager.managers.IDataManager
import com.cartrack.challenge.dataModel.usersAuthenticationDataModel.UserAuthObj
import io.reactivex.Observable

class LoginRepo(private val mIDataManager: IDataManager) {
    fun validateUser(email: String, password: String): Observable<UserAuthObj> {
        return mIDataManager.retrieveUser(password, email)
    }

    fun registerNewUser(user: UserAuthObj): Observable<Boolean> {
       return mIDataManager.registerUser(user)
    }

}