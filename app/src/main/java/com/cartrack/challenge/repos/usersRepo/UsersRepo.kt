package com.cartrack.challenge.repos.usersRepo

import androidx.lifecycle.LiveData
import com.cartrack.challenge.dataManager.managers.IDataManager
import com.cartrack.challenge.dataModel.usersDataModel.UserObj
import com.cartrack.challenge.networking.utilities.ApiResponse
import com.cartrack.challenge.networking.utilities.NetworkBoundResource
import com.cartrack.challenge.networking.utilities.Resource

class UsersRepo(private val mIDataManager: IDataManager) {

    fun loadUsersData(pageNo: String): LiveData<Resource<List<UserObj>>> {
        return object :
            NetworkBoundResource<List<UserObj>, List<UserObj>>(mIDataManager.getAppExecutors()) {
            override fun saveCallResult(items: List<UserObj>) {
                mIDataManager.insertUsers(items)
            }

            override fun shouldFetch(data: List<UserObj>): Boolean {
                if (data.isEmpty())
                    return true
                return false
            }

            override fun loadFromDb(): LiveData<List<UserObj>> {
                return mIDataManager.retrieveUsers()
            }

            override fun createCall(): LiveData<ApiResponse<List<UserObj>>> {
                return mIDataManager.loadUsersList(pageNo)
            }

        }.asLiveData()
    }
}