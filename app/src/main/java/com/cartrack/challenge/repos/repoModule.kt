package com.cartrack.challenge.repos

import com.cartrack.challenge.repos.loginRepo.LoginRepo
import com.cartrack.challenge.repos.usersRepo.UsersRepo
import org.koin.dsl.module

val repoModule = module {

    /**
     * provide user repo with koin
     * */
    single { UsersRepo(get()) }
    single { LoginRepo(get()) }

}