package com.cartrack.challenge.networking.services

import androidx.lifecycle.LiveData
import com.cartrack.challenge.dataModel.usersDataModel.UserObj
import com.cartrack.challenge.networking.utilities.ApiResponse

class LiveDataApiServiceHelper(private val mLiveDataApiService: LiveDataApiService) :
    LiveDataApiService {
    override fun loadUsersList(pageNo: String): LiveData<ApiResponse<List<UserObj>>> {
        return mLiveDataApiService.loadUsersList(pageNo)
    }

}