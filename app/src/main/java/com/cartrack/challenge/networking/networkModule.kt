package com.cartrack.challenge.networking

import com.cartrack.challenge.BuildConfig
import com.cartrack.challenge.utilities.AppConstants
import com.cartrack.challenge.networking.liveDataUtilities.LiveDataCallAdapterFactory
import com.cartrack.challenge.networking.services.LiveDataApiService
import com.cartrack.challenge.networking.services.LiveDataApiServiceHelper
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.StringQualifier
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.net.ssl.HttpsURLConnection

val networkModule = module {

    /**
     * lifeData Retrofit service for the project
     * [StringQualifier] to specify name for the definition to distinguish it.
     * This is the main app [Retrofit] resolved  by name default
     */
    single<Retrofit>(StringQualifier(AppConstants.LIFE_DATA_RETROFIT)) {
        Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(get())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .build()
    }
    single<OkHttpClient> {

        val httpClient = OkHttpClient().newBuilder()
            .connectTimeout(AppConstants.REQUEST_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(AppConstants.REQUEST_TIMEOUT, TimeUnit.SECONDS)
            .hostnameVerifier { hostname, session ->
                val hv = HttpsURLConnection.getDefaultHostnameVerifier()
                hv.verify(hostname, session)
            }
            .writeTimeout(AppConstants.REQUEST_TIMEOUT, TimeUnit.SECONDS)
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        httpClient.addInterceptor(interceptor)
        httpClient.build()

    }
    /**
     * provide lifeData Service
     * */
    single {
        provideLifeDataRetrofitService(get(StringQualifier(AppConstants.LIFE_DATA_RETROFIT)))
    }
    /**
     * provide lifeData Service implementation
     * */
    single { LiveDataApiServiceHelper(get()) }
}

fun provideLifeDataRetrofitService(retrofit: Retrofit): LiveDataApiService =
    retrofit.create(LiveDataApiService::class.java)
