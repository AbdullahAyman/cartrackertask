package com.cartrack.challenge.networking.services

import androidx.lifecycle.LiveData
import com.cartrack.challenge.dataModel.usersDataModel.UserObj
import com.cartrack.challenge.networking.utilities.ApiResponse
import retrofit2.http.GET
import retrofit2.http.Header

interface LiveDataApiService {

    @GET("/users")
    fun loadUsersList(
        @Header("pageNo") pageNo: String
    ): LiveData<ApiResponse<List<UserObj>>>

}