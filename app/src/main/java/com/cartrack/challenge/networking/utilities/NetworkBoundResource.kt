package com.cartrack.challenge.networking.utilities

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

abstract class NetworkBoundResource<ResponseType, DBResultType>
@MainThread constructor(private val appExecutors: AppExecutors) {

    private val result = MediatorLiveData<Resource<DBResultType>>()

    init {
        result.value = Resource.loading(null)
        @Suppress("LeakingThis")
        val dbSource = loadFromDb()
        result.addSource(dbSource) { data ->
            if (data != null) {
                if (shouldFetch(data)) {
                    result.removeSource(dbSource)
                    fetchFromNetwork(dbSource)
                }
                setValue(Resource.success(data))
            } else {
                result.removeSource(dbSource)
                fetchFromNetwork(dbSource)
            }
        }
    }

    @MainThread
    private fun setValue(newValue: Resource<DBResultType>) {
        if (result.value != newValue) {
            result.value = newValue
        }
    }

    private fun fetchFromNetwork(dbSource: LiveData<DBResultType>) {
        val apiResponse = createCall()
        // we re-attach dbSource as a new phoneNo, it will dispatch its latest value quickly
        result.addSource(dbSource) { newData ->
            setValue(Resource.loading(newData))
        }


        result.addSource(apiResponse) { response ->
            result.removeSource(apiResponse)
            result.removeSource(dbSource)
            when (response) {
                is ApiSuccessResponse -> {
                    appExecutors.diskIO().execute {
                        saveCallResult(processResponse(response.body))
                        appExecutors.mainThread().execute {
                            // we specially request a new live data,
                            // otherwise we will get immediately last cached value,
                            // which may not be updated with latest userReposListItems received from network.
                            result.addSource(loadFromDb()) { newData ->
                                setValue(Resource.success(newData))
                            }
                        }
                    }
                }
                is ApiEmptyResponse -> {
                    appExecutors.mainThread().execute {
                        // reload from disk whatever we had
                        result.addSource(loadFromDb()) { newData ->
                            setValue(Resource.success(newData))
                        }
                    }
                }
                is ApiErrorResponse -> {
                    onFetchFailed(response.errorMessage)
                    result.addSource(dbSource) { newData ->
                        setValue(Resource.error(response.errorMessage, newData))
                    }
                }
            }
        }
    }

    protected open fun onFetchFailed(errorMessage: String) {}

    fun asLiveData() = result as LiveData<Resource<DBResultType>>

    @WorkerThread
    protected open fun processResponse(response: ResponseType) = response

    @WorkerThread
    protected abstract fun saveCallResult(items: ResponseType)

    @MainThread
    protected abstract fun shouldFetch(data: DBResultType): Boolean

    @MainThread
    protected abstract fun loadFromDb(): LiveData<DBResultType>

    @MainThread
    protected abstract fun createCall(): LiveData<ApiResponse<ResponseType>>
}
