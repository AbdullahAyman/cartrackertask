package com.cartrack.challenge.networking.utilities

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}