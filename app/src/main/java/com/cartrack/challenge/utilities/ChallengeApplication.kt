package com.cartrack.challenge.utilities

import android.app.Application
import com.cartrack.challenge.BuildConfig
import com.cartrack.challenge.dataManager.dataModule
import com.cartrack.challenge.networking.networkModule
import com.cartrack.challenge.repos.repoModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class ChallengeApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        setUpTimber()
        setUpKoin()
    }

    /**
     * start Timber in case of debugging.
     */
    private fun setUpTimber() {
        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
    }

    /**
     * initialize and start Koin.
     */
    private fun setUpKoin() {

        startKoin {
            if (BuildConfig.DEBUG)
                androidLogger()

            androidContext(this@ChallengeApplication)

            modules(
                appModule,
                networkModule,
                dataModule,
                repoModule
            )
        }
    }

}