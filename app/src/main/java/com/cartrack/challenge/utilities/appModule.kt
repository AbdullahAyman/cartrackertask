package com.cartrack.challenge.utilities

import androidx.recyclerview.widget.LinearLayoutManager
import com.cartrack.challenge.views.landingActivity.LandingActivityViewModel
import com.cartrack.challenge.views.landingActivity.usersList.adapter.UserListAdapter
import com.cartrack.challenge.views.landingActivity.usersList.viewModel.UsersViewModel
import com.cartrack.challenge.views.registerActivity.RegisterActivityViewModel
import io.reactivex.disposables.CompositeDisposable
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module


val appModule = module {

    single { CompositeDisposable() }

    factory { LinearLayoutManager(get()) }

    viewModel { UsersViewModel(get()) }
    viewModel { LandingActivityViewModel() }

    viewModel { RegisterActivityViewModel(get()) }

    single(named("CompositeDisposable")) { CompositeDisposable() }

    single { UserListAdapter() }
}