package com.cartrack.challenge.utilities

class AppConstants {
    companion object {
        const val REQUEST_TIMEOUT = 20L
        /**
         *  indicator for life data service
         * */
        const val LIFE_DATA_RETROFIT = "lifeDataRetrofitService"

        const val DB_NAME = "ChallengeDB"
    }
}