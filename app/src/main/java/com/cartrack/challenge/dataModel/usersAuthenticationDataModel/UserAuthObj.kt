package com.cartrack.challenge.dataModel.usersAuthenticationDataModel

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class UserAuthObj(
    @PrimaryKey(autoGenerate = true)
    var id: Int? = 1,
    var password: String?,
    var email: String?,
    var country: String?
)