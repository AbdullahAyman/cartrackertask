package com.cartrack.challenge.dataModel.usersDataModel

import android.os.Parcelable
import androidx.room.Entity
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class Company(
    @SerializedName("name")
    var name: String?,
    @SerializedName("catchPhrase")
    @Expose
    var catchPhrase: String?,
    @SerializedName("bs")
    @Expose
    var bs: String?
) : Parcelable