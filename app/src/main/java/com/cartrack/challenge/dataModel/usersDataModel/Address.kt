package com.cartrack.challenge.dataModel.usersDataModel

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class Address(
    @SerializedName("street")
    var street: String?,
    @SerializedName("suite")
    var suite: String?,
    @SerializedName("city")
    var city: String?,
    @SerializedName("zipcode")
    var zipcode: String?,
    @Embedded(prefix = "loc_")
    @SerializedName("geo")
    var geo: Geo?
) : Parcelable