package com.cartrack.challenge.views.landingActivity.usersList.viewModel

import androidx.databinding.ObservableField
import com.cartrack.challenge.dataModel.usersDataModel.UserObj
import com.cartrack.challenge.views.landingActivity.usersList.ItemClickListener

class UserItemViewModel(dataItem: UserObj, listener: ItemClickListener) {

    private var userItem: UserObj = dataItem
    private var itemListener: ItemClickListener = listener

    var name: ObservableField<String> = ObservableField()
    var phoneNo: ObservableField<String> = ObservableField()
    var address: ObservableField<String> = ObservableField()
    var webSite: ObservableField<String> = ObservableField()

    init {
        name.set(userItem.name)
        phoneNo.set(userItem.phone)
        address.set(userItem.address?.zipcode + ", " + userItem.address?.street)
        webSite.set(userItem.website)
    }

    fun openItemWebSite() {
        itemListener.openWebSite(userItem)
    }

    fun openItemLocation() {
        itemListener.openLocation(userItem)
    }
}