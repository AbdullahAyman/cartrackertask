package com.cartrack.challenge.views.landingActivity.usersList

import com.cartrack.challenge.dataModel.usersDataModel.UserObj

interface ItemClickListener {

    fun openLocation(userItem: UserObj)
    fun openWebSite(userItem: UserObj)
}