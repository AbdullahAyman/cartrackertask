package com.cartrack.challenge.views.registerActivity

import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.Observer
import com.cartrack.challenge.BR
import com.cartrack.challenge.R
import com.cartrack.challenge.base.BaseActivity
import com.cartrack.challenge.databinding.ActivityRegisterBinding
import com.cartrack.challenge.views.landingActivity.LandingActivity
import org.koin.android.ext.android.inject

class RegisterActivity : BaseActivity<ActivityRegisterBinding, RegisterActivityViewModel>() {
    private val mViewModel: RegisterActivityViewModel by inject()
    private lateinit var mViewBinding: ActivityRegisterBinding

    override val viewModel: RegisterActivityViewModel
        get() = mViewModel
    override val bindingVariable: Int
        get() = BR.viewModel
    override val layoutId: Int
        get() = R.layout.activity_register

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewBinding = viewDataBinding!!
        mViewModel.getCommand().observe(this, Observer {
            it.getContentIfNotHandled()?.let { it ->
                when (it) {
                    is RegisterActivityCommand.NavigateToLandingView -> {
                        displayLandingScreen()
                    }
                    is RegisterActivityCommand.displayToast -> {
                        displayToastMessage(it.message)
                    }
                    is RegisterActivityCommand.emailErrorMessage -> {
                        mViewBinding.registerTvEmail.error = it.message
                    }
                    is RegisterActivityCommand.passwordErrorMessage -> {
                        mViewBinding.registerTvPassword.error = it.message
                    }
                    is RegisterActivityCommand.countryErrorMessage -> {
                        displayToastMessage(it.message!!)
                    }
                    is RegisterActivityCommand.countryList -> {
                        val arrayAdapter = ArrayAdapter<String>(
                            this,
                            android.R.layout.simple_spinner_dropdown_item,
                            it.list
                        )
                        mViewBinding.registerSpinnerCountries.adapter = arrayAdapter
                    }
                }
            }
        })
        mViewModel.getSpinnerItems()
    }

    private fun displayToastMessage(message: String) {
        Toast.makeText(this@RegisterActivity, message, Toast.LENGTH_LONG).show()
    }

    private fun displayLandingScreen() {
        startActivity(Intent(this@RegisterActivity, LandingActivity::class.java))
    }
}