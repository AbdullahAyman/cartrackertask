package com.cartrack.challenge.views.landingActivity.usersList.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cartrack.challenge.BR
import com.cartrack.challenge.R
import com.cartrack.challenge.base.BaseFragment
import com.cartrack.challenge.dataModel.usersDataModel.UserObj
import com.cartrack.challenge.databinding.FragmentUserListBinding
import com.cartrack.challenge.networking.utilities.Status
import com.cartrack.challenge.views.landingActivity.usersList.ItemClickListener
import com.cartrack.challenge.views.landingActivity.usersList.adapter.UserListAdapter
import com.cartrack.challenge.views.landingActivity.usersList.viewModel.UsersViewModel
import com.google.android.material.snackbar.Snackbar
import org.koin.android.ext.android.inject
import timber.log.Timber

class UsersFragment : BaseFragment<FragmentUserListBinding, UsersViewModel>(), ItemClickListener {

    private val mViewModel: UsersViewModel by inject()
    private val mUserListAdapter: UserListAdapter by inject()
    private lateinit var mViewBinding: FragmentUserListBinding
    private val mLinearLayoutManager: LinearLayoutManager by inject()

    companion object {
        fun newInstance() = UsersFragment()
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_user_list
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getViewModel(): UsersViewModel {
        return mViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewBinding = getViewDataBinding()
        setupRecycler()
        mViewModel.usersData?.observe(viewLifecycleOwner, Observer { result ->
            when (result?.status) {
                Status.SUCCESS -> {
                    Timber.v("Status.SUCCESS")
                    val list = result.data
                    mViewModel.loadingVisibility.set(false)
                    mUserListAdapter.assignItems(list!!)
                }
                Status.ERROR -> {
                    Timber.v("Status.ERROR")
                    Timber.v(result.message)
                    showSnackBar(result.message, Snackbar.LENGTH_LONG)
                    mViewModel.loadingVisibility.set(false)
                }
                Status.LOADING -> {
                    Timber.v("Status.LOADING")
                    mViewModel.loadingVisibility.set(true)
                }

            }
        })
        mViewModel.reload()
    }

    private fun setupRecycler() {
        mLinearLayoutManager.orientation = RecyclerView.VERTICAL
        mViewBinding.usersRecycler.layoutManager = mLinearLayoutManager
        mViewBinding.usersRecycler.itemAnimator = DefaultItemAnimator()
        mViewBinding.usersRecycler.adapter = mUserListAdapter
        mUserListAdapter.assignListener(this)
    }

    override fun openLocation(userItem: UserObj) {
        val intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse("geo:${userItem.address?.city} ?q=${userItem.address?.geo?.lng}, ${userItem.address?.geo?.lat}")
        )
        startActivity(intent)
    }

    override fun openWebSite(userItem: UserObj) {
        val intent  =  Intent(Intent.ACTION_VIEW, Uri.parse(userItem.website))
        context?.startActivity(Intent.createChooser(intent, "Choose browser"))
    }

}