package com.cartrack.challenge.views.landingActivity.usersList.adapter

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cartrack.challenge.base.BaseViewHolder
import com.cartrack.challenge.dataModel.usersDataModel.UserObj
import com.cartrack.challenge.databinding.ItemUserBinding
import com.cartrack.challenge.views.landingActivity.usersList.ItemClickListener
import com.cartrack.challenge.views.landingActivity.usersList.viewModel.UserItemViewModel


class UserListAdapter : RecyclerView.Adapter<BaseViewHolder>() {
    var mUsers: ArrayList<UserObj> = ArrayList()
    lateinit var mListener: ItemClickListener

    fun assignListener(itemListener: ItemClickListener) {
        mListener = itemListener
    }

    fun assignItems(items: List<UserObj>) {
        mUsers.clear()
        mUsers.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return UsersAdapterViewHolder(ItemUserBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return mUsers.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }


    inner class UsersAdapterViewHolder(itemView: ItemUserBinding) : BaseViewHolder(itemView.root) {
        private var mViewBinding: ItemUserBinding = itemView
        private lateinit var mViewModel: UserItemViewModel

        override fun onBind(position: Int) {
            mViewModel = UserItemViewModel(mUsers[position], mListener)
            mViewBinding.viewModel = mViewModel
            mViewBinding.executePendingBindings()
            mViewBinding.userWebsiteTv.paintFlags = Paint.UNDERLINE_TEXT_FLAG
            mViewBinding.userUserAddressTv.paintFlags = Paint.UNDERLINE_TEXT_FLAG
        }

    }


}