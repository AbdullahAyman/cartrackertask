package com.cartrack.challenge.views.registerActivity

sealed class RegisterActivityCommand {
    object NavigateToLandingView : RegisterActivityCommand()
    class displayToast(val message: String) : RegisterActivityCommand()
    class emailErrorMessage(val message: String?) : RegisterActivityCommand()
    class passwordErrorMessage(val message: String?) : RegisterActivityCommand()
    class countryErrorMessage(val message: String?) : RegisterActivityCommand()
    class countryList(val list: ArrayList<String>) : RegisterActivityCommand()

}