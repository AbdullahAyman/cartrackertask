package com.cartrack.challenge.views.registerActivity

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.cartrack.challenge.base.BaseViewModel
import com.cartrack.challenge.dataModel.usersAuthenticationDataModel.UserAuthObj
import com.cartrack.challenge.repos.loginRepo.LoginRepo
import com.cartrack.challenge.utilities.Event
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.collections.ArrayList

class RegisterActivityViewModel(private val mLoginRepo: LoginRepo) : BaseViewModel() {

    open var loading = ObservableField<Boolean>(false)
    private var email = ObservableField<String>("")
    private var password = ObservableField<String>("")
    private var selectedCountryPosition = ObservableField(0)
    private var mRegisterActivityCommand: MutableLiveData<Event<RegisterActivityCommand>> = MutableLiveData()

    var countryList = ArrayList<String>()

    fun onEmailNameTextChanged(charSequence: CharSequence) {
        email.set(charSequence.toString())
        mRegisterActivityCommand.value =
            Event(RegisterActivityCommand.emailErrorMessage(null))
    }

    fun onPasswordTextChanged(charSequence: CharSequence) {
        password.set(charSequence.toString())
        mRegisterActivityCommand.value =
            Event(RegisterActivityCommand.passwordErrorMessage(null))
    }

    fun onCountryItemSelected(position: Int) {
        selectedCountryPosition.set(position)
    }

    fun getCommand(): LiveData<Event<RegisterActivityCommand>> {
        return mRegisterActivityCommand
    }

    fun loginUser() {
        //TODO Validate the user credentials and proceed to the next screen
        if (isEmailValid(email.get()!!))
            if (isPasswordValid(password.get()!!))
                if (selectedCountryPosition.get()!! > 0) {
                    loading.set(true)
                    disposable().add(
                        mLoginRepo.validateUser(email.get()!!, password.get()!!)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ response ->
                                if (response != null) {
                                    //TODO navigate to landing screen
                                    mRegisterActivityCommand.value =
                                        Event(RegisterActivityCommand.NavigateToLandingView)
                                    mRegisterActivityCommand.value =
                                        Event(RegisterActivityCommand.displayToast("Login Success!"))
                                    loading.set(false)
                                } else {
                                    registerNewUser()
                                }
                            }, { error ->
                                Timber.d("User Not Registered! ${error.message}")
                                mRegisterActivityCommand.value =
                                    Event(RegisterActivityCommand.displayToast("User Not Registered!"))
                                registerNewUser()
                                loading.set(false)
                            })
                    )
                } else
                    mRegisterActivityCommand.value =
                        Event(RegisterActivityCommand.displayToast("Select a country!"))
            else
                mRegisterActivityCommand.value =
                    Event(RegisterActivityCommand.passwordErrorMessage("Type complex P@ssw0rd !"))
        else
            mRegisterActivityCommand.value =
                Event(RegisterActivityCommand.emailErrorMessage("Type Valid Email!"))


    }

    private fun registerNewUser() {
        val mUserAuthObj = UserAuthObj(
            password = password.get(),
            email = email.get(),
            country = countryList[selectedCountryPosition.get()!!]
        )
        disposable().add(
            mLoginRepo.registerNewUser(mUserAuthObj)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    if (response) {
                        Timber.d("User Registered!")
                        //TODO navigate to landing screen
                        mRegisterActivityCommand.value =
                            Event(RegisterActivityCommand.displayToast("Registration succeeded!"))
                        mRegisterActivityCommand.value =
                            Event(RegisterActivityCommand.NavigateToLandingView)
                    } else {
                        Timber.d("Registration Failed!")
                        mRegisterActivityCommand.value =
                            Event(RegisterActivityCommand.displayToast("Registration failed!"))
                    }
                    loading.set(false)
                }, { error ->
                    Timber.d("User Not Registered! ${error.message}")
                    mRegisterActivityCommand.value =
                        Event(RegisterActivityCommand.displayToast("Registration failed!"))
                    loading.set(false)
                })
        )
    }

    private fun isEmailValid(email: String): Boolean {
        val pattern: Pattern
        val matcher: Matcher
        val EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        pattern = Pattern.compile(EMAIL_PATTERN)
        matcher = pattern.matcher(email)
        return matcher.matches()
    }

    private fun isPasswordValid(password: String): Boolean {
        val PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[@#$%]).{8,})"
        val pattern: Pattern
        val matcher: Matcher
        pattern = Pattern.compile(PASSWORD_PATTERN)
        matcher = pattern.matcher(password)
        return matcher.matches()
    }


    fun getSpinnerItems() {
        val locale = Locale.getAvailableLocales()
        val countries = ArrayList<String>()
        var country: String
        for (loc in locale) {
            country = loc.displayCountry
            if (country.isNotEmpty() && !countries.contains(country)) {
                countries.add(country)
            }
        }
        Collections.sort(countries, String.CASE_INSENSITIVE_ORDER)
        countries.add(0, "-- Select Country --")
        countryList.clear()
        countryList.addAll(countries)
        mRegisterActivityCommand.value =
            Event(RegisterActivityCommand.countryList(countries))
    }
}