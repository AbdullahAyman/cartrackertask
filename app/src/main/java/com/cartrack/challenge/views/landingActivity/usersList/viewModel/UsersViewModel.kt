package com.cartrack.challenge.views.landingActivity.usersList.viewModel

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.cartrack.challenge.base.BaseViewModel
import com.cartrack.challenge.dataModel.usersDataModel.UserObj
import com.cartrack.challenge.networking.liveDataUtilities.AbsentLiveData
import com.cartrack.challenge.networking.utilities.Resource
import com.cartrack.challenge.repos.usersRepo.UsersRepo

class UsersViewModel(private val mUsersRepo: UsersRepo) : BaseViewModel() {
    private var mDataResponse: MutableLiveData<Boolean> = MutableLiveData()
    var loadingVisibility: ObservableField<Boolean> = ObservableField(false)
    var pageNo: Int = 0
    val usersData: LiveData<Resource<List<UserObj>>>? = Transformations
        .switchMap(mDataResponse) {
            if (it == false) {
                AbsentLiveData.create()
            } else {
                mUsersRepo.loadUsersData(pageNo.toString())
            }
        }

    fun reload() {
        loadingVisibility.set(true)
        mDataResponse.postValue(true)
    }
}