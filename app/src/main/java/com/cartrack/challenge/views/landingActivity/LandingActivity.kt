package com.cartrack.challenge.views.landingActivity

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.cartrack.challenge.BR
import com.cartrack.challenge.R
import com.cartrack.challenge.base.BaseActivity
import com.cartrack.challenge.databinding.ActivityMainBinding
import com.cartrack.challenge.views.landingActivity.usersList.view.UsersFragment
import org.koin.android.ext.android.inject

class LandingActivity : BaseActivity<ActivityMainBinding, LandingActivityViewModel>() {
    private val mViewModel: LandingActivityViewModel by inject()


    override val viewModel: LandingActivityViewModel
        get() = mViewModel
    override val bindingVariable: Int
        get() = BR.viewModel
    override val layoutId: Int
        get() = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadFragment(UsersFragment.newInstance())
    }

    private fun loadFragment(fragment: Fragment) {

        supportFragmentManager.beginTransaction().also {
            it.replace(R.id.main_frame, fragment)
            it.commitNowAllowingStateLoss()
        }

    }
}
