package com.cartrack.challenge.dataManager.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cartrack.challenge.dataModel.usersAuthenticationDataModel.UserAuthObj

@Dao
interface UsersAuthenticationDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun registerUser(user: UserAuthObj)

    @Query("SELECT * FROM UserAuthObj where email=:email AND password=:password")
    fun retrieveUser(password: String, email: String): UserAuthObj

}