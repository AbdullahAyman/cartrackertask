package com.cartrack.challenge.dataManager.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.cartrack.challenge.dataModel.usersAuthenticationDataModel.UserAuthObj
import com.cartrack.challenge.dataModel.usersDataModel.UserObj

@Database(
    entities = [
        UserObj::class,
        UserAuthObj::class],
    version = 2,
    exportSchema = false
)
abstract class ChallengeDB : RoomDatabase() {
    abstract fun usersDataDao(): UsersDAO

    abstract fun usersAuthenticationDao(): UsersAuthenticationDAO
}