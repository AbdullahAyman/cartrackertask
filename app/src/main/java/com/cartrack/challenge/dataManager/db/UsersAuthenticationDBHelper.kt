package com.cartrack.challenge.dataManager.db

import com.cartrack.challenge.dataModel.usersAuthenticationDataModel.UserAuthObj
import io.reactivex.Observable

interface UsersAuthenticationDBHelper {
    fun registerUser(user: UserAuthObj): Observable<Boolean>

    fun retrieveUser(password: String, email: String): Observable<UserAuthObj>
}