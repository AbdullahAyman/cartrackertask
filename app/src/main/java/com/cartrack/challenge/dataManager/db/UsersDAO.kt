package com.cartrack.challenge.dataManager.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cartrack.challenge.dataModel.usersDataModel.UserObj

@Dao
interface UsersDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUsers(user: List<UserObj>)

    @Query("SELECT * FROM UserObj")
    fun retrieveUsers(): LiveData<List<UserObj>>

    @Query("DELETE FROM UserObj")
    fun deleteAllUsers()
}