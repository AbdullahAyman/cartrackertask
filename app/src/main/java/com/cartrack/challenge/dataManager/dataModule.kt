package com.cartrack.challenge.dataManager

import android.app.Application
import androidx.room.Room
import com.cartrack.challenge.dataManager.db.ChallengeDB
import com.cartrack.challenge.dataManager.managers.DataManagerImpl
import com.cartrack.challenge.dataManager.managers.IDataManager
import com.cartrack.challenge.networking.utilities.AppExecutors
import com.cartrack.challenge.utilities.AppConstants
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import java.util.concurrent.Executors

val dataModule = module {

    /**
     * providing AppExecutors for limited threads
     * */
    single {
        AppExecutors(
            Executors.newSingleThreadExecutor(),
            Executors.newFixedThreadPool(3),
            AppExecutors.MainThreadExecutor()
        )
    }

    /**
     * providing db
     * */
    single { provideChallengeDB(androidApplication()) }

    single<IDataManager> {
        DataManagerImpl(
            get(),
            get(),
            get()
        )
    }

}

fun provideChallengeDB(app: Application): ChallengeDB {
    return Room
        .databaseBuilder(app, ChallengeDB::class.java, AppConstants.DB_NAME)
        .fallbackToDestructiveMigration()
        .build()
}