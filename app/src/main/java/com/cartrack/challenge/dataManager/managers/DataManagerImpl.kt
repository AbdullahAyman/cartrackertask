package com.cartrack.challenge.dataManager.managers

import androidx.lifecycle.LiveData
import com.cartrack.challenge.dataManager.db.ChallengeDB
import com.cartrack.challenge.dataModel.usersAuthenticationDataModel.UserAuthObj
import com.cartrack.challenge.dataModel.usersDataModel.UserObj
import com.cartrack.challenge.networking.services.LiveDataApiService
import com.cartrack.challenge.networking.utilities.ApiResponse
import com.cartrack.challenge.networking.utilities.AppExecutors
import io.reactivex.Observable

class DataManagerImpl(
    private val mLiveDataApiService: LiveDataApiService,
    private val mChallengeDB: ChallengeDB,
    private val appExecutors: AppExecutors
) : IDataManager {
    override fun getAppExecutors(): AppExecutors {
        return appExecutors
    }

    override fun insertUsers(user: List<UserObj>) {
        mChallengeDB.usersDataDao().insertUsers(user)
    }

    override fun retrieveUsers(): LiveData<List<UserObj>> {
        return mChallengeDB.usersDataDao().retrieveUsers()
    }

    override fun deleteAllUsers() {
        mChallengeDB.usersDataDao().deleteAllUsers()
    }

    override fun loadUsersList(pageNo: String): LiveData<ApiResponse<List<UserObj>>> {
        return mLiveDataApiService.loadUsersList(pageNo)
    }

    override fun retrieveUser(password: String, email: String): Observable<UserAuthObj> {
        return Observable.fromCallable {
            mChallengeDB.usersAuthenticationDao().retrieveUser(password, email)
        }
    }

    override fun registerUser(user: UserAuthObj): Observable<Boolean> {
        return Observable.fromCallable {
            mChallengeDB.usersAuthenticationDao().registerUser(user)
            true
        }

    }
}