package com.cartrack.challenge.dataManager.managers

import com.cartrack.challenge.dataManager.db.DBInterface
import com.cartrack.challenge.networking.services.LiveDataApiService
import com.cartrack.challenge.networking.utilities.AppExecutors

interface IDataManager : DBInterface, LiveDataApiService{
    fun getAppExecutors(): AppExecutors
}